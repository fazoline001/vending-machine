#pragma checksum "C:\Users\darat\source\repos\Vending Machine\Vending Machine\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7c77c2df91f72e28f660d2fde2666618b0c0402d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\darat\source\repos\Vending Machine\Vending Machine\Views\_ViewImports.cshtml"
using Vending_Machine;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\darat\source\repos\Vending Machine\Vending Machine\Views\_ViewImports.cshtml"
using Vending_Machine.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7c77c2df91f72e28f660d2fde2666618b0c0402d", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1c93de087a6e33f278af5bdccacb7b0da7406b6f", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "none", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "empty", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\darat\source\repos\Vending Machine\Vending Machine\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"text-left\">\r\n    <h1 class=\"display-4\">Insert the money</h1>\r\n    <select id=\"moneys\" onchange=\"getSnacks(this.selectedIndex)\">\r\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7c77c2df91f72e28f660d2fde2666618b0c0402d4047", async() => {
                WriteLiteral("\r\n            None\r\n        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n    </select>\r\n    <select id=\"snacks\" style=\"visibility:hidden;margin-top:auto\" onchange=\"priceChange(this.selectedIndex)\">\r\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7c77c2df91f72e28f660d2fde2666618b0c0402d5376", async() => {
                WriteLiteral("None");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
    </select>
    </br>
    <div id=""infoDiv"" style=""visibility:hidden"">
        <label>Price :&nbsp;</label><label id=""price"" >0</label>&nbsp;<label>Quantity :&nbsp;</label><label id=""qty"">0</label>
    </div>
    <input type=""button"" onclick=""Submit()"" value=""Buy!""/>&nbsp;<label id=""alert"" style=""visibility:hidden"">Please choose the snack!</label>
</div>

");
            DefineSection("Scripts", async() => {
                WriteLiteral(@"
    <script>
        var alls = Array();
        var noms = Array();
        $.get(window.location + ""Nominals/All"", function (datas) {
            noms = datas.value;
            $.each(datas.value, function (i, data) {
                $('#moneys').append($('<option>', {
                    value: data.id,
                    text: data.priceNominal
                }));
            });
        });

        function getSnacks(id) {
            console.log(""ini dari snacks ""+id);
            if (document.getElementById(""moneys"").options[id].value !== ""none"") {
                $('#snacks').prop('style', 'visibility:visible');
                $('#infoDiv').prop('style', 'visibility:visible');
                $.get(window.location + ""Items/All"", function (datas) {
                    alls = datas.value;
                    $.each(datas.value, function (i, data) {
                        $('#snacks').append($('<option>', {
                            value: data.id,
                       ");
                WriteLiteral(@"     text: data.name
                        }));
                    });
                });
            } else {
                $('#snacks').prop('style', 'visibility:hidden');
                $('#snacks').empty();
                $('#infoDiv').prop('style', 'visibility:hidden');
            }
        }

        function priceChange(id) {

            for (i = 0; i < alls.length; i++) {
                if (alls[i].id === document.getElementById(""snacks"").options[id].value) {
                    document.getElementById('price').innerHTML = alls[i].price;
                    document.getElementById('qty').innerHTML = alls[i].qty;
                    break;
                } else {
                    document.getElementById('price').innerHTML = 0;
                    document.getElementById('qty').innerHTML = 0;
                }
            }
        }

        function Submit() {
            var monId = document.getElementById('moneys').options[document.getElementById('moneys').se");
                WriteLiteral(@"lectedIndex].value;
            var priId = document.getElementById('snacks').options[document.getElementById('snacks').selectedIndex].value;

            if (monId == ""none"") {
                $('#alert').prop('style', 'visibility:visible');
                document.getElementById('alert').innerHTML = ""Insufficient Money!"";
            } else {
                if (priId == ""empty"") {
                    $('#alert').prop('style', 'visibility:visible');
                    document.getElementById('alert').innerHTML = ""Please choose the snack!"";
                } else {
                    if (alls.length > 0 && alls[document.getElementById('snacks').selectedIndex-1].qty > 0) {
                        $('#alert').prop('style', 'visibility:hidden');
                        var jobject = {
                            ""nomId"": monId,
                            ""snaId"": priId
                        };
                        console.log(jobject);
                        $.ajax({
               ");
                WriteLiteral(@"             method: 'POST',
                            url: window.location + 'Items/Bought',
                            dataType: 'application/json',
                            data: jobject,
                            complete: function (result) {
                                var data = JSON.parse(result.responseText);
                                console.log(data);
                                $('#alert').prop('style', 'visibility:visible');
                                document.getElementById('alert').innerHTML = data.value.messageStatus + "" "" + data.value.change;
                                document.getElementById('moneys').value = 'none';
                                getSnacks(0);
                            }
                        });
                    } else {
                        $('#alert').prop('style', 'visibility:visible');
                        document.getElementById('alert').innerHTML = ""Snack Unavailable!"";
                    }
              ");
                WriteLiteral("      \r\n                }\r\n\r\n            }\r\n            \r\n        }\r\n    </script>\r\n    ");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
