﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vending_Machine.Data
{
    public class CommonCons
    {
        public string ItemScript()
        {
            return " CREATE TABLE vending_machine.\"Item\"(" + "id uuid NOT NULL," +
                    "name character varying(255) COLLATE pg_catalog.\"default\"," +
                     "\"qty\" integer , " +
                     "\"price\" bigint , " +
                    "CONSTRAINT vm_pkey PRIMARY KEY (id))" 
                    ;
        }
        public string NominalScript()
        {
            return " CREATE TABLE vending_machine.\"Nominal\"(" + "id uuid NOT NULL," +
                     "\"priceNominal\" bigint , " +
                    "CONSTRAINT nm_pkey PRIMARY KEY (id))"
                    ;
        }
    }
}
