﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vending_Machine.Models;

namespace Vending_Machine.Data
{
    public class VendingMachineContext :DbContext
    {
        public VendingMachineContext(DbContextOptions<VendingMachineContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("vending_machine");
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Vending_Machine.Models.Item> Item { get; set; }

        public DbSet<Vending_Machine.Models.Nominal> Nominal { get; set; }
    }
}
