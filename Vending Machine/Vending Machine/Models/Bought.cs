﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vending_Machine.Models
{
    public class Bought
    {
        public Guid nomId { get; set; }
        public Guid snaId { get; set; }
        public int change { get; set; }
        public string messageStatus { get; set; }
    }
}
