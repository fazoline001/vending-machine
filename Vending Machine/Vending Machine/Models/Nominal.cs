﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Vending_Machine.Models
{
    public class Nominal
    {
        [Key]
        public Guid id { get; set; }
        public int priceNominal { get; set; }
    }
}
