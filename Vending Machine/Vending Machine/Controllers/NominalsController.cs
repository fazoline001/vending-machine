﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Vending_Machine.Data;
using Vending_Machine.Models;

namespace Vending_Machine.Controllers
{
    public class NominalsController : Controller
    {
        private readonly VendingMachineContext _context;

        public NominalsController(VendingMachineContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("Nominals/All")]
        public async Task<ActionResult> getAll()
        {

            return Ok(Json(_context.Nominal.ToList()));
        }
        // GET: Nominals
        public async Task<IActionResult> Index()
        {
            try
            {
                _context.Database.ExecuteSqlCommand(new CommonCons().NominalScript());
                return View(await _context.Nominal.ToListAsync());
            }
            catch
            {
                return View(await _context.Nominal.ToListAsync());
            }
        }

        // GET: Nominals/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nominal = await _context.Nominal
                .FirstOrDefaultAsync(m => m.id == id);
            if (nominal == null)
            {
                return NotFound();
            }

            return View(nominal);
        }

        // GET: Nominals/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Nominals/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,priceNominal")] Nominal nominal)
        {
            if (ModelState.IsValid)
            {
                nominal.id = Guid.NewGuid();
                _context.Add(nominal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(nominal);
        }

        // GET: Nominals/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nominal = await _context.Nominal.FindAsync(id);
            if (nominal == null)
            {
                return NotFound();
            }
            return View(nominal);
        }

        // POST: Nominals/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("id,priceNominal")] Nominal nominal)
        {
            if (id != nominal.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(nominal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NominalExists(nominal.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(nominal);
        }

        // GET: Nominals/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nominal = await _context.Nominal
                .FirstOrDefaultAsync(m => m.id == id);
            if (nominal == null)
            {
                return NotFound();
            }

            return View(nominal);
        }

        // POST: Nominals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var nominal = await _context.Nominal.FindAsync(id);
            _context.Nominal.Remove(nominal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NominalExists(Guid id)
        {
            return _context.Nominal.Any(e => e.id == id);
        }
    }
}
