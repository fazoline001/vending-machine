﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Vending_Machine.Data;
using Vending_Machine.Models;

namespace Vending_Machine.Controllers
{
    public class ItemsController : Controller 
    {
        private readonly VendingMachineContext _context;

        public ItemsController(VendingMachineContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("Items/All")]
        public async Task<ActionResult> getAll()
        {

            return Ok(Json(_context.Item.ToList()));
        }

        [HttpPost]
        [Route("Items/Bought")]
        public async Task<IActionResult> bought([FromForm]Bought data)
        {
            Nominal noms = _context.Nominal.Find(data.nomId);
            Item items = _context.Item.Find(data.snaId);
            if (ModelState.IsValid)
            {
                
                try
                {
                    data.change = noms.priceNominal - items.price;
                    if (data.change > 0)
                    {
                        items.qty--;
                        _context.Update(items);
                        await _context.SaveChangesAsync();
                        data.messageStatus = "Success!";
                    }
                    else
                    {
                        data.messageStatus = "Insufficient Money!";
                    }
                    
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists(items.id))
                    {
                        data.messageStatus = "Snack Doesn't Exist! Are you try to cheating?";
                    }
                    else
                    {
                        data.messageStatus = "Nominal Doesn't Exist! Are you try to cheating?";
                    }
                }
            }
            return Ok(Json(data));
        }

        // GET: Items
        public async Task<IActionResult> Index()
        {
            try
            {
                _context.Database.ExecuteSqlCommand(new CommonCons().ItemScript());
                return View(await _context.Item.ToListAsync());
            }
            catch
            {
                return View(await _context.Item.ToListAsync());
            }
        }

        // GET: Items/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Item
                .FirstOrDefaultAsync(m => m.id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // GET: Items/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,qty,price")] Item item)
        {
            if (ModelState.IsValid)
            {
                item.id = Guid.NewGuid();
                _context.Add(item);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(item);
        }

        // GET: Items/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Item.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            return View(item);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("id,name,qty,price")] Item item)
        {
            if (id != item.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(item);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists(item.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(item);
        }

        // GET: Items/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Item
                .FirstOrDefaultAsync(m => m.id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // POST: Items/Delete/5
        [Microsoft.AspNetCore.Mvc.HttpPost, Microsoft.AspNetCore.Mvc.ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var item = await _context.Item.FindAsync(id);
            _context.Item.Remove(item);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemExists(Guid id)
        {
            return _context.Item.Any(e => e.id == id);
        }
    }

}
